﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.IO;
using System.Windows.Forms;

namespace ScytaleC
{
    public partial class LHFForm : Form
    {
        private bool sendingLine;
        private bool readerInitialized;
        private TextReader reader;
        private UdpTx udpTx;

        public LHFForm()
        {
            InitializeComponent();

            sendingLine = false;
            readerInitialized = false;
            udpTx = new UdpTx();
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                readerInitialized = false;
            }
        }

        private void btnSendNext_Click(object sender, EventArgs e)
        {
            if (sendingLine) return;

            if (!readerInitialized) InitializeReader();

            sendingLine = true;

            try
            {
                //read next line
                if (reader.Peek() > 0)
                {
                    string line = reader.ReadLine();

                    string[] parts = line.Split(' ');
                    if (parts.Length > 1)
                    {
                        string sid = parts[0];
                        string frame = parts[1];

                        //udp
                        byte[] bsid = BitConverter.GetBytes(Convert.ToInt64(sid));
                        sid = Utils.BytesToHexString(bsid, 0, bsid.Length);
                        udpTx.Send(txtUdpAddress.Text, (int)nUdpPort.Value, Utils.HexStringToBytes(frame + sid));

                        //display
                        byte[] bframe = Utils.HexStringToBytes(frame);
                        int frameNumber = bframe[2] << 8 | bframe[3];
                        richTextBox1.AppendText(string.Format("{0}  ({1}) {2}", frameNumber, sid, frame));
                    }
                    else
                    {
                        richTextBox1.AppendText(line);
                    }
                    richTextBox1.AppendText(Environment.NewLine);
                    richTextBox1.ScrollToCaret();
                }
            }
            catch (Exception ex)
            {
                richTextBox1.AppendText(Environment.NewLine);
                richTextBox1.AppendText(ex.Message);
            }
            finally
            {
                sendingLine = false;
            }
        }

        private void InitializeReader()
        {
            if (reader != null) reader.Close();
            reader = File.OpenText(textBox1.Text);

            readerInitialized = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (checkBox1.Checked) btnSendNext_Click(null, null);
        }

        private void LHFForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (reader != null) reader.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            readerInitialized = false;
        }
    }
}
