﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 *   
 * 92 - Login Acknowledgement
 *  
 * Acknowledge single-----------------
 * Very possible that the SES contains other info like: stationID, name, capabilities...
 * 9207AIEDDE2B484F
 * 6069 - 92: Login ACK     -- SES: A1EDDE  NCS FRQ: 1537.700 MHz (2B48)
 * 
 * 9207AIEDDE2B484F
 * 92
 *    07               Login Ack Length
 *     AIEDDE          Les
 *           2B48      Frequency
 *               47    ?
 * 
 * Acknowledge multiple---------------
 * 922DC57FE82B48BF0601F8F7F9FFFF02F8FF792E2E03F8FF792E6204F8FFF92E320CF8FFF92E3A15F8FFF92E22xxxx
 * 6070 - 92: Login ACK     -- SES: C57FE8  NCS FRQ: 1537.700 MHz (2B48)
 *    001 [6OICO ASCT-DCFPLA2D--L] ----.--- Telenor Satellite Services Inc
 *    002 [6OICO ASCTHDCF-LA2D--L] 1539.555 Stratos Burum-2 Netherlands
 *    003 [6OICO ASCTHDCF-LA2D--L] 1539.685 Yamaguchi Japan
 *    004 [6OICO ASCTHDCFPLA2D--L] 1539.565 Southbury USA
 *    012 [6OICO ASCTHDCFPLA2D--L] 1539.585 Station 12 Netherlands
 *    021 [6OICO ASCTHDCFPLA2D--L] 1539.525 France Telecom MSC
 *    
 *  92          
 *  2D                  Login Ack Length
 *  C57FE8              Les
 *  2B48                Frequency
 *  BF                  ? acts like an information separator
 *  
 *  06                  StationCount
 *  01 F8 F7F9 FFFF     ID, Status, Services, Frequency, Name(ID)   
 *  02 F8 FF79 2E2E
 *  03 F8 FF79 2E62
 *  04 F8 FFF9 2E32
 *  0C F8 FFF9 2E3A
 *  15 F8 FFF9 2E22
 *  xxxx                CRC
 * 
 *  The sequence above was decoded watching the display output of the DEMO version of the std-c decoder
 *  available at http://www.inmarsatdecoder.com/ 
 */

using ScytaleC.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoder92 : PacketDecoder
    {
        public int LoginAckLength { get; set; }
        public string Les { get; set; }
        public double DownlinkChannelMHz { get; set; }
        public string StationStartHex { get; set; }
        public int StationCount { get; set; }
        public List<PacketDecoderUtils.Station> Stations { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("92");
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                LoginAckLength = args.DescrambledFrame[pos + 1];
                Les = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 3);
                if (args.DescrambledFrame[pos + 5] == 0xFF && args.DescrambledFrame[pos + 5] == 0xFF)
                {
                    DownlinkChannelMHz = 0;
                }
                else
                {
                    // (2-byte value - 8000) * 0.0025 + 1530 HHz
                    DownlinkChannelMHz = PacketDecoderUtils.ReturnDownlinkChannelMHz(args.DescrambledFrame, pos + 5);
                }

                StationStartHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 7, 1);

                if (LoginAckLength > 7)
                {
                    //stations
                    StationCount = args.DescrambledFrame[pos + 8];
                    Stations = new List<PacketDecoderUtils.Station>();

                    int j = pos + 9;
                    for (int i = 0; i < StationCount; i++)
                    {
                        PacketDecoderUtils.Station s = PacketDecoderUtils.ReturnStation(args.DescrambledFrame, j);
                        Stations.Add(s);

                        j += 6;
                    }
                }
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}