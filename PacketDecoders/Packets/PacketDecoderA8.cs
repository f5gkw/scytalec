﻿/*
 * microp11, holger 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 *   
 * 
 * A8 - Confirmation
 * 
 * The A8 packet can contain additional data. Usually this data is encodet in IA5.
 * 
 * A8 0B 803404 42 0758E1 02 81 7719
 * 
 * A8 27 5FA54C 4C 03BE0A 1E 81 E361F2ECEF73AE73E9EC766140F4F2616E73E96E7375EC61F2AE70F4F3EF
 * A8 2A 5FA54C 4C 03BE0A 21 81 61ECE5F8616E64F2E5AE73E9EC766140F4F2616E73E96E7375EC61F2AE70F466C6
 * A8 19 66607E 44 0EDD6B 10 81 756DE9F440616B6E75F2AE6EE5F45BE0
 * A8 20 66607E 44 0EDD6B 17 81 E56E67AE6D75F46173E56D40616B6E75F2AE6EE5F48AFE
 * A8 1A 66607E 44 0EDD6B 11 81 EF7073407661F2616D61F2AEE3EF6D786D
 *    |  |      |  |      |  |  |
 *    |  |      |  |      |  |  short Message
 *    |  |      |  |      |  unknown, however it looks like a packet descriptor, might go with the sender id?
 *    |  |      |  |      Length of additional data (including this length byte)
 *    |  |      |  unknown (could be a senderID)
 *    |  |      LES_Id
 *    |  MES_Id
 *    Length of complete packet
 * 
 * 
 * */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;
using System.Text;

namespace ScytaleC.PacketDecoders
{
    public class PacketDecoderA8 : PacketDecoder
    {
        public int MesId { get; set; }
        public string MesIdHex { get; set; }
        public int Sat { get; set; }
        public string SatName { get; set; }
        public int LesId { get; set; }
        public string LesName { get; set; }
        public string Unknown1Hex { get; set; }
        public int ShortMessageLength { get; set; }
        public string Unknown2Hex { get; set; }
        public string ShortMessage { get; set; }

        public override void Decode(DescrambledFrameArgs args, ref int pos)
        {
            Debug.WriteLine("A8");
            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Partial;

                MesId = PacketDecoderUtils.ReturnMesId(args.DescrambledFrame, pos + 2);
                MesIdHex = Utils.BytesToHexString(args.DescrambledFrame, pos + 2, 3);

                Sat = PacketDecoderUtils.ReturnSat(args.DescrambledFrame[pos + 5]);
                SatName = PacketDecoderUtils.ReturnSatName(Sat);

                LesId = PacketDecoderUtils.ReturnLesId(args.DescrambledFrame[pos + 5]);
                LesName = PacketDecoderUtils.ReturnLesName(Sat, LesId);

                Unknown1Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 6, 3);

                ShortMessageLength = args.DescrambledFrame[pos + 9];

                Unknown2Hex = Utils.BytesToHexString(args.DescrambledFrame, pos + 10, 1);

                if (ShortMessageLength > 2)
                {
                    byte[] textPayload = new byte[PacketLength - 13];
                    int j = pos + 11;
                    int payloadLength = 0;
                    for (int i = 0; j < pos + PacketLength - 2; i++)
                    {
                        textPayload[i] = (byte)(args.DescrambledFrame[j] & 0x7F);
                        j++;
                        payloadLength++;
                    }
                    Encoding enc = Encoding.GetEncoding("x-IA5",
                                          new EncoderExceptionFallback(),
                                          new DecoderExceptionFallback());
                    ShortMessage = enc.GetString(textPayload, 0, textPayload.Length);
                }
                else
                {
                    ShortMessage = "";
                }

            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }

    }
}

