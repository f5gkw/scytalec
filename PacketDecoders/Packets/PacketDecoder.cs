﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
* 
*   Bibliography:
* 
*   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
*   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
*   
*   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
*   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
* 
*   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
*   http://docslide.us/documents/tt3026-software-reference-manual.html
*   
*   
*/

using ScytaleC.Interfaces;
using System;
using System.Web.Script.Serialization;

namespace ScytaleC.PacketDecoders
{
    public static class DecodingStage
    {
        public static int None = 0;
        public static int Partial = 1;
        public static int Complete = 2;
    }

    public class Payload
    {
        public int Presentation { get; set; }
        public string PresentationName { get; set; }
        public string Ia5 { get; set; }
        public string Ita2 { get; set; }
        public byte[] Data8Bit { get; set; }
    }

    public class PacketDecoder
    {
        /// <summary>
        /// The Id and StreamId are not needed at the packet level, however
        /// they help identify the origin of the stream containing the packet.
        /// 
        /// Also the FrameNumber is not part of the packet, but this way it is linked to the originating packet.
        /// The Time is calculated based on the StreamId and the FrameNumber
        /// </summary>
        public int Id { get; set; }
        public long StreamId { get; set; }
        public int FrameNumber { get; set; }
        public DateTime Time { get; set; }

        public bool IsHeaderOnly { get; set; }
        public byte PacketDescriptor { get; set; }
        public string PacketDescriptorHex { get; set; }
        public byte CommandType { get; set; }
        public int PacketLength { get; set; }
        public string HexSequence { get; set; }
        public int DecodingStage_ { get; set; }
        public string CrashReport { get; set; }
        public bool IsCRC { get; set; }
        public Payload Payload_ { get; set; }

        public PacketDecoder()
        {
            DecodingStage_ = DecodingStage.None;
            Payload_ = new Payload
            {
                //The AA packets do not include a Presentation.
                //We set the default Presentation to -1 = Unknown
                Presentation = -1
            };
        }

        public virtual string Serialize()
        {
            return new JavaScriptSerializer().Serialize(this);
        }

        /// <summary>
        /// The PacketLength includes the CRC
        /// </summary>
        /// <param name="args"></param>
        /// <param name="pos"></param>
        public virtual void Decode(DescrambledFrameArgs args, ref int pos)
        {
            IsHeaderOnly = false;

            /// As a safe precaution, in case we cannot correctly ascertain the packet length,
            /// setting the length this way will allow us to discard the whole remaining frame
            /// and avoid an infinite loop.
            PacketLength = 640 - pos;

            /// Packet descriptor
            PacketDescriptor = args.DescrambledFrame[pos];
            PacketDescriptorHex = Utils.BytesToHexString(args.DescrambledFrame, pos, 1);

            CommandType = (byte)((PacketDescriptor & 0x70) >> 4);

            /// There are 2 variations of packet descriptor
            /// Short packet descriptor and Medium packet descriptor
            /// They give us the packet lengths
            if (PacketDescriptor >> 7 == 0)
            {
                /// Short packet descriptor
                /// The packet length including CRC does not include byte 0, we add 1
                PacketLength = (PacketDescriptor & 0x0F) + 1;
            }
            else if (PacketDescriptor >> 6 == 0x02)
            {
                /// Medium packet descriptor
                /// The packet length including CRC does not include the first 2 bytes, we add 2
                PacketLength = args.DescrambledFrame[pos + 1] + 2;
            }

            /// At this stage we do not know for sure if the CRC is correct.
            /// As such there is the possibility to run out of bounds while computing the packet data.
            try
            {
                HexSequence = Utils.BytesToHexString(args.DescrambledFrame, pos, PacketLength);

                /// We compute the 2-byte CRC and compare with the packet 2-byte CRC
                /// The packet 2-byte CRC position is given by the packet descriptor
                int packetCRC = (args.DescrambledFrame[pos + PacketLength - 2] << 8) | args.DescrambledFrame[pos + PacketLength - 1];
                int computedCRC = PacketDecoderUtils.ComputeCRC(args.DescrambledFrame, pos, PacketLength);

                //Added a check to zero.
                //The BD-BE packet content is a packet that does not have a CRC.
                //This is a workaround to avoid computing the CRC for the content.
                //This needs be fixed by actually computing the CRC. TODO
                IsCRC = packetCRC == 0 || packetCRC == computedCRC;
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
                Utils.Log.Error(ex);

                /// We also set the CRC to "bad" and we do not continue decoding this packet.
                IsCRC = false;
            }
        }
    }
}
