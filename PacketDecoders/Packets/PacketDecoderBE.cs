﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Bibliography:
 * 
 *   Proesch, R., & Daskalaki-Proesch, A. (2013). Technical Handbook for Radio Monitoring VHF/UHF. Norderstedt, Germany: Books on Demand GmbH.
 *   https://www.amazon.ca/Technical-Handbook-Radio-Monitoring-VHF/dp/3732241475/
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 *   Thrane & Thrane. (2006) TT-3026 easyTrack Transceiver Software Interface Reference Manual. Thrane & Thrane A/S
 *   http://docslide.us/documents/tt3026-software-reference-manual.html
 * 
 *  
 * BE07ADB0B9AE3111
 * 
 * BE   - frame type
 * 07   - Size
 * ADB0B9AE3111 - payload
 * 
 * This frame contains a continuation of the payload of the previous Multiframe Message frame.
 * 
 */

using ScytaleC.Interfaces;
using System;
using System.Diagnostics;

namespace ScytaleC.PacketDecoders

{
    /// <summary>
    /// BE - Multiframe Message Continue
    /// </summary>
    public class PacketDecoderBE : PacketDecoder
    {
        //default constructor needed for dynamic casting
        public PacketDecoderBE() { }

        public void Decode(DescrambledFrameArgs args, ref int pos, ref MultiFramePacket mfa)
        {
            Debug.WriteLine("BE");

            try
            {
                base.Decode(args, ref pos);
                if (!IsCRC)
                {
                    return;
                }

                DecodingStage_ = DecodingStage.Complete;

                //2 for CRC
                //2 for starting of packet
                byte[] payload = new byte[PacketLength - 2 - 2];
                int j = pos + 2;
                int actualLength = 0;
                for (int i = 0; j < pos + PacketLength - 2; i++)
                {
                    payload[i] = args.DescrambledFrame[j];
                    j++;
                    actualLength++;
                }
                Array.Copy(payload, 0, mfa.PacketData, mfa.FirstPartCount, actualLength);
                mfa.FirstPartCount += actualLength;
                mfa.IsReady = mfa.FirstPartCount == mfa.PacketData.Length - 2;

                Payload_.Data8Bit = new byte[actualLength];
                Array.Copy(payload, Payload_.Data8Bit, actualLength);
                Debug.WriteLine("... of an actual length of {0}", actualLength);
            }
            catch (Exception ex)
            {
                CrashReport = string.Format("{0} [{1}]", ex.Message, ex.StackTrace);
            }
        }
    }
}