﻿namespace ScytaleC
{
    partial class FftDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                bitmap.Dispose();
                neutralPen.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbldB = new System.Windows.Forms.Label();
            this.lblCF = new System.Windows.Forms.Label();
            this.volumeBar = new System.Windows.Forms.ProgressBar();
            this.box = new System.Windows.Forms.PictureBox();
            this.scatter = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scatter)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbldB
            // 
            this.lbldB.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbldB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbldB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbldB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbldB.Location = new System.Drawing.Point(87, 35);
            this.lbldB.Name = "lbldB";
            this.lbldB.Size = new System.Drawing.Size(79, 25);
            this.lbldB.TabIndex = 10;
            this.lbldB.Text = "0.0 dB";
            this.lbldB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCF
            // 
            this.lblCF.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblCF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCF.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblCF.Location = new System.Drawing.Point(172, 10);
            this.lblCF.Name = "lblCF";
            this.lblCF.Padding = new System.Windows.Forms.Padding(1);
            this.lblCF.Size = new System.Drawing.Size(49, 50);
            this.lblCF.TabIndex = 11;
            this.lblCF.Text = "2000.0 Hz";
            this.lblCF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // volumeBar
            // 
            this.volumeBar.Location = new System.Drawing.Point(87, 10);
            this.volumeBar.Maximum = 10000;
            this.volumeBar.Name = "volumeBar";
            this.volumeBar.Size = new System.Drawing.Size(79, 20);
            this.volumeBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.volumeBar.TabIndex = 8;
            this.volumeBar.Value = 63;
            // 
            // box
            // 
            this.box.BackColor = System.Drawing.SystemColors.ControlLight;
            this.box.Location = new System.Drawing.Point(1, 1);
            this.box.Name = "box";
            this.box.Size = new System.Drawing.Size(343, 67);
            this.box.TabIndex = 9;
            this.box.TabStop = false;
            this.box.Paint += new System.Windows.Forms.PaintEventHandler(this.Box_Paint);
            this.box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_MouseDown);
            this.box.MouseLeave += new System.EventHandler(this.box_MouseLeave);
            this.box.MouseMove += new System.Windows.Forms.MouseEventHandler(this.box_MouseMove);
            // 
            // scatter
            // 
            this.scatter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.scatter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.scatter.Location = new System.Drawing.Point(10, 1);
            this.scatter.Name = "scatter";
            this.scatter.Size = new System.Drawing.Size(66, 64);
            this.scatter.TabIndex = 12;
            this.scatter.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.lblCF);
            this.panel1.Controls.Add(this.scatter);
            this.panel1.Controls.Add(this.volumeBar);
            this.panel1.Controls.Add(this.lbldB);
            this.panel1.Location = new System.Drawing.Point(344, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 67);
            this.panel1.TabIndex = 13;
            // 
            // FftDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.box);
            this.Name = "FftDisplay";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(572, 69);
            ((System.ComponentModel.ISupportInitialize)(this.box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scatter)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lbldB;
        internal System.Windows.Forms.Label lblCF;
        private System.Windows.Forms.ProgressBar volumeBar;
        internal System.Windows.Forms.PictureBox box;
        private System.Windows.Forms.PictureBox scatter;
        private System.Windows.Forms.Panel panel1;
    }
}
