﻿/*
 * microp11 2017
 * 
 * Source:
 * http://stackoverflow.com/questions/18813112/naudio-fft-result-gives-intensity-on-all-frequencies-c-sharp
 * 
 */

using NAudio.Dsp;
using System;
using System.Diagnostics;

namespace ScytaleC
{
    class SampleAggregator
    {
        // FFT
        public event EventHandler<FftEventArgs> FftCalculated;
        public bool PerformFFT { get; set; }

        private Complex[] fftBuffer;
        private FftEventArgs fftArgs;
        private int fftPos;
        private int fftLength;
        private int m;
        private Stopwatch sw;
        private int count;

        public SampleAggregator(int fftLength)
        {
            if (!IsPowerOfTwo(fftLength))
            {
                throw new ArgumentException("FFT Length must be a power of two.");
            }
            this.m = (int)Math.Log(fftLength, 2.0);
            this.fftLength = fftLength;
            this.fftBuffer = new Complex[fftLength];
            this.fftArgs = new FftEventArgs(fftBuffer);

            sw = new Stopwatch();
        }

        bool IsPowerOfTwo(int x)
        {
            return x > 0 && (x & (x - 1)) == 0;
        }

        public void Add(float value)
        {
            if (PerformFFT && FftCalculated != null)
            {
                count--;
                if (count > fftLength)
                {
                    return;
                }
                // Re
                fftBuffer[fftPos].X = (float)(value * FastFourierTransform.HammingWindow(fftPos, fftLength));

                // Im is zero with audio.
                fftBuffer[fftPos].Y = 0;

                fftPos++;
                if (fftPos >= fftLength)
                {
                    //get a counter so we run one fft / second.
                    fftPos = 0;
                    FastFourierTransform.FFT(true, m, fftBuffer);
                    FftCalculated?.Invoke(this, fftArgs);
                    //Debug.WriteLine("FFTDONE at count{0} {1}s", count, sw.ElapsedMilliseconds);
                    //sw.Restart();
                    count = (int)Math.Floor(fftLength * 2.0);
                }
            }
        }
    }

    public class FftEventArgs : EventArgs
    {
        public FftEventArgs(Complex[] result)
        {
            this.Result = result;
        }
        public Complex[] Result { get; set; }
    }
}
