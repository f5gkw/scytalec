﻿/*
 * microp11 2017
 * 
 * 
 * Displays the FFT, volume, intensity, center fequency, and lock/unlock demodulator state.
 * 
 * http://www.vb-helper.com/howto_net_draw_flipped_text.html
 * 
 * Feb 2019: Mille DVB <milledvb1@gmail.com>: FftDisplay.cs patched to display fft in logarithmic form and to calculate SNR correctly
 */

using ScytaleC.Decoder;
using ScytaleC.PacketDecoders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Numerics;
using System.Windows.Forms;

namespace ScytaleC
{
    public partial class FftDisplay : UserControl
    {
        internal SampleAggregator sampleAggregator = new SampleAggregator(DataConsts.SamplesPerRead);
        internal float scaleX = 1.0F;
        internal float scaleY = 1.0F;
        internal List<PointF> graphPoints = new List<PointF>();
        internal Bitmap bitmap;
        internal float maxVal = 100;
        internal Point[] mouseBox = new Point[4];
        internal Point[] centerFreqBox = new Point[4];
        internal Point[] loFreqBox = new Point[4];
        internal Point[] hiFreqBox = new Point[4];
        internal Color outlinePenColor = Color.Red;

        internal Graphics g;
        internal Complex[] scatterPoints;
        internal Pen neutralPen;
        internal Point[] scatterBox = new Point[4];
        internal Color outlineScatterBoxPen = Color.White;
        internal double multiplicator;
        internal bool lastSymbolFinderAlgo;

        private Stopwatch sw;
        private int labelRefreshCount;
        private double[] binFrequencies;
        private int binFrequenciesCount;
        private double[] frequencyByPixel;
        private int[] pixelByFrequency;
        private double mousePositionFrequency;

        private int loFreq;
        private int hiFreq;

        // A 10th of the bins are 48000 sample rate is 4800Hz
        // We only display this much.
        private int fftDisplayDivisor = 10;
        private int lastSignalFrequency = 2000;

        private Demodulator demodulator;

        public FftDisplay()
        {
            InitializeComponent();
            InitializeFFT();
            InitializeBitmap();
            InitializeScatter();

            sw = new Stopwatch();

            binFrequenciesCount = DataConsts.SamplesPerRead / fftDisplayDivisor;
            binFrequencies = new double[binFrequenciesCount];
            ComputeBinFrequencies(DataConsts.AudioSampleRate / fftDisplayDivisor, binFrequenciesCount);

            frequencyByPixel = new double[box.Width];
            pixelByFrequency = new int[DataConsts.AudioSampleRate / fftDisplayDivisor];
            ComputePixelFrequencies(box.Width);

            loFreq = DataConsts.LoFrequency;
            hiFreq = DataConsts.HiFrequency;
        }

        private void InitializeScatter()
        {
            g = scatter.CreateGraphics();
            scatterPoints = new Complex[100];
            neutralPen = new Pen(SystemColors.ControlLight);
        }

        private void InitializeBitmap()
        {
            bitmap = new Bitmap(box.Width, box.Height);
            box.Image = bitmap;
        }

        private void InitializeFFT()
        {
            sampleAggregator.FftCalculated += SampleAggregator_FftCalculated;
            sampleAggregator.PerformFFT = true;
        }

        private void ComputeBinFrequencies(double sampleRate, int bins)
        {

            for (int i = 0; i <  bins; i++)
            {
                binFrequencies[i] = i * sampleRate / bins;
            }
        }

        private void ComputePixelFrequencies(int pixels)
        {
            int j;
            int k = 0;
            for (int i = 0; i <  pixels; i++)
            {
                j = i * binFrequenciesCount / pixels;
                frequencyByPixel[i] = binFrequencies[j];

                for (; k < binFrequencies[j]; k++)
                {
                    pixelByFrequency[k] = i;
                }
            }
        }

        internal void SetDemodulator(Demodulator demodulator)
        {
            this.demodulator = demodulator;
        }

        /// <summary>
        /// https://stackoverflow.com/questions/4364823/how-do-i-obtain-the-frequencies-of-each-value-in-an-fft
        /// https://groups.google.com/forum/#!topic/comp.dsp/cZsS1ftN5oI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SampleAggregator_FftCalculated(object sender, FftEventArgs e)
        {
            //Debug.WriteLine("SampleAggregator_FftCalculated {0}", sw.ElapsedMilliseconds);
            //sw.Restart();

            BeginInvoke(new Action(() =>
            {
                graphPoints.Clear();

                double maxdB = -100;

                double[] mag = new double[e.Result.Length / fftDisplayDivisor];
                double[] A = new double[e.Result.Length / fftDisplayDivisor];

                // resolution
                double resolution = (double)(DataConsts.AudioSampleRate / fftDisplayDivisor) / (double)(e.Result.Length / fftDisplayDivisor);
                
                // bandwith of BPSK 1200 = 1200Hz
                int noise_index_low = (int)((lastSignalFrequency - 600) / resolution);
                int noise_index_high = (int)((lastSignalFrequency + 600) / resolution);

                int cnt_noise = 0;
                int cnt_signal = 0;
                double noise = 0;
                double signal = 0;


                maxVal = Int32.MinValue;
                
                for (int k = 0; k < e.Result.Length; k++)
                {
                    if (k < e.Result.Length / fftDisplayDivisor)
                    {
                        double dB = 10 * Math.Log10(4 * (e.Result[k].X * e.Result[k].X + e.Result[k].Y * e.Result[k].Y) / ((e.Result.Length / fftDisplayDivisor) * (e.Result.Length / fftDisplayDivisor)));
                        // mag[k] = 2 * Math.Sqrt(e.Result[k].X * e.Result[k].X + e.Result[k].Y * e.Result[k].Y) / e.Result.Length;

                        mag[k] = 2 * Math.Sqrt(e.Result[k].X * e.Result[k].X + e.Result[k].Y * e.Result[k].Y) / (e.Result.Length / fftDisplayDivisor);
                        
                        A[k] = 20 * Math.Log10(mag[k]);
                        //graphPoints.Add(new PointF(k, (float)(100 + A[k])));
                        graphPoints.Add(new PointF(k, (float)(mag[k])));

                        // remove DC
                        if (k > 2)
                        {
                            maxVal = Math.Max(maxVal, (float)mag[k]);
                        }

                        // for SNR calculation
                        if ((k <= noise_index_low) || (k >= noise_index_high))
                        {
                            noise += mag[k];
                            cnt_noise++;
                        }
                        else
                        {
                            signal += mag[k];
                            cnt_signal++;
                        }

                        maxdB = Math.Max(maxdB, dB);
                    }

                }

                // TODO: set SNR to 0 if carrier is not detected
                maxdB = 20 * Math.Log10((signal / cnt_signal) / (noise / cnt_noise));
                maxdB = Math.Max(0, maxdB);
                lbldB.Text = string.Format("{0:0.0} dB", maxdB);

                scaleX = (float)bitmap.Width / graphPoints.Count;
                scaleY = bitmap.Height / maxVal;
                box.Invalidate();
            }));
        }

        private void Box_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                e.Graphics.Transform = new Matrix(1, 0, 0, -1, 0, bitmap.Height);
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

                if (graphPoints != null && graphPoints.Count > 1)
                {
                    GraphicsPath gOutlinePath = new GraphicsPath();
                    gOutlinePath.FillMode = FillMode.Winding;
                    gOutlinePath.AddLines(graphPoints.ToArray());
                    gOutlinePath.Transform(new Matrix(scaleX, 0, 0, scaleY, 0, 0));
                    Pen matrixPen = new Pen(outlinePenColor, 1);
                    matrixPen.DashStyle = DashStyle.Solid;
                    try
                    {
                        e.Graphics.DrawPath(matrixPen, gOutlinePath);
                    }
                    catch (OverflowException oe)
                    {
                        Debug.WriteLine(oe.Message);
                    }
                    gOutlinePath.Dispose();

                    GraphicsPath gSignalPath = new GraphicsPath();
                    gSignalPath.AddLines(graphPoints.ToArray());
                    gSignalPath.Transform(new Matrix(scaleX, 0, 0, (float)(scaleY * 0.97), 0, 0));
                    try
                    {
                        e.Graphics.DrawPath(Pens.White, gSignalPath);
                    }
                    catch (OverflowException oe)
                    {
                        Debug.WriteLine(oe.Message);
                    }
                    gSignalPath.Dispose();
                }

                // cf path
                GraphicsPath cfPath = new GraphicsPath();
                cfPath.AddPolygon(centerFreqBox);
                Pen cfPen = new Pen(outlinePenColor, 1);
                cfPen.DashStyle = DashStyle.Dash;
                e.Graphics.DrawPath(cfPen, cfPath);

                //range path
                GraphicsPath rangePath = new GraphicsPath();
                rangePath.AddPolygon(loFreqBox);
                rangePath.AddPolygon(hiFreqBox);
                Pen pathPen = new Pen(Color.BlueViolet, 3);
                pathPen.DashStyle = DashStyle.Solid;
                e.Graphics.DrawPath(pathPen, rangePath);

                // mouse path
                GraphicsPath mousePath = new GraphicsPath();
                mousePath.AddPolygon(mouseBox);
                Pen mousePen = new Pen(Color.DarkSlateBlue, 1);
                mousePen.DashStyle = DashStyle.Dash;
                e.Graphics.DrawPath(mousePen, mousePath);
                if (mousePositionFrequency > 0)
                {
                    e.Graphics.ScaleTransform(1, -1);
                    e.Graphics.DrawString(string.Format("{0:0.0}", mousePositionFrequency), new Font("Tahoma", 10), Brushes.BlueViolet, 5, -20);
                    e.Graphics.ResetTransform();
                }
            }
            catch(Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void box_MouseDown(object sender, MouseEventArgs e)
        {
            demodulator?.SetCenterFrequency(frequencyByPixel[e.X]);
        }

        //Hide the box when leaving the control
        private void box_MouseLeave(object sender, EventArgs e)
        {
            mouseBox[0] = new Point(0, 0);
            mouseBox[1] = new Point(0, 0);
            mouseBox[2] = new Point(0, 0);
            mouseBox[3] = new Point(0, 0);
            mousePositionFrequency = -1;
        }

        //Show the box when inside control, hide horizontal lines of the box
        private void box_MouseMove(object sender, MouseEventArgs e)
        {
            mouseBox[0] = new Point(e.X - 10, 0);
            mouseBox[1] = new Point(e.X + 10, 0);
            mouseBox[2] = new Point(e.X + 10, box.Height + 5);
            mouseBox[3] = new Point(e.X - 10, box.Height + 5);
            if (e.X < box.Image.Width)
            {
                //Debug.WriteLine("x: {0}, y: {1}, frequency: {2}", e.X, e.Y, frequencyByPixel[e.X]);
                mousePositionFrequency = frequencyByPixel[e.X];
            }
        }

        internal void SetSignalAttributes(SignalAttributesArgs e)
        {
            try
            {
                if (double.IsNaN(e.Frequency))
                {
                    return;
                }

                labelRefreshCount++;
                box.Invalidate();

                if (e.IsCMAEnabled)
                {
                    multiplicator = 15;
                }
                else
                {
                    multiplicator = 2.5;
                }

                // we need to remove all traces of the previous algo scatter points
                // otherwise we won't be able to clean them up
                if (lastSymbolFinderAlgo != e.IsCMAEnabled)
                {
                    scatter.Invalidate();
                }
                lastSymbolFinderAlgo = e.IsCMAEnabled;

                if (labelRefreshCount > 6)
                {
                    labelRefreshCount = 0;
                    lblCF.Text = string.Format("{0:0.0} Hz", e.Frequency);

                    // not the greatest way to calculate signal volume
                    int v = e.MeanMagnitude;
                    if (v < volumeBar.Minimum)
                    {
                        v = volumeBar.Minimum;
                    }
                    else if (v > volumeBar.Maximum)
                    {
                        v = volumeBar.Maximum;
                    }
                    volumeBar.Value = v;
                }

                lastSignalFrequency = (int)e.Frequency;
                int transF = pixelByFrequency[(int)e.Frequency];
                centerFreqBox[0] = new Point(transF, 5);
                centerFreqBox[1] = new Point(transF, 5);
                centerFreqBox[2] = new Point(transF, box.Height - 5);
                centerFreqBox[3] = new Point(transF, box.Height - 5);

                loFreqBox[0] = new Point(pixelByFrequency[loFreq], 2);
                loFreqBox[1] = new Point(pixelByFrequency[loFreq], 2);
                loFreqBox[2] = new Point(pixelByFrequency[loFreq], 10);
                loFreqBox[3] = new Point(pixelByFrequency[loFreq], 10);

                hiFreqBox[0] = new Point(pixelByFrequency[hiFreq], 2);
                hiFreqBox[1] = new Point(pixelByFrequency[hiFreq], 2);
                hiFreqBox[2] = new Point(pixelByFrequency[hiFreq], 10);
                hiFreqBox[3] = new Point(pixelByFrequency[hiFreq], 10);

                if (e.IsInSync)
                {
                    outlinePenColor = Color.Green;
                }
                else
                {
                    outlinePenColor = Color.Red;
                }

                DrawScatterPoint(e.ScatterPoint);
                g.DrawRectangle(Pens.DarkGray, new Rectangle(1, 1, 61, 62));
            }
            catch (IndexOutOfRangeException oore)
            {
                Debug.WriteLine(oore.Message);
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void DrawScatterPoint(Complex scatterPoint)
        {
            if (scatterPoint.Real > 10 || double.IsNaN(scatterPoint.Real) ||
                scatterPoint.Imaginary > 10 || double.IsNaN(scatterPoint.Imaginary))
            {
                return;
            }

            for (int i = 0; i < 100 - 1; i++)
            {
                scatterPoints[i] = scatterPoints[i + 1];
            }
            scatterPoints[99] = scatterPoint;
            float x = 0;
            float y = 0;
            ScalePointToCanvas(scatterPoints[0], ref x, ref y);

            g.DrawEllipse(neutralPen, x, y, 5, 5);
            ScalePointToCanvas(scatterPoints[89], ref x, ref y);
            g.DrawEllipse(Pens.ForestGreen, x, y, 5, 5);
            ScalePointToCanvas(scatterPoints[99], ref x, ref y);
            g.DrawEllipse(Pens.White, x, y, 5, 5);
        }

        private void ScalePointToCanvas(Complex point, ref float center_x, ref float center_y)
        {
            try
            {
                center_x = 28 - (float)(point.Real * multiplicator);
                center_y = 30 + (float)(point.Imaginary * multiplicator);
            }
            catch
            {
                center_x = 0;
                center_y = 0;
            }
        }

        internal void SetLoFreq(int loFreq)
        {
            this.loFreq = loFreq;
        }

        internal void SetHiFreq(int hiFreq)
        {
            this.hiFreq = hiFreq;
        }
    }
}
