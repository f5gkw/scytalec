﻿/*
 * microp11 2017
 * 
 *  
 * This class (naively written) provides a consistent output, to be used as input for a demodulator, fft,...
 * It is a smart queue. Everything equeued is guaranteed to be 16bit, LE, double, mono, PCM.
 * The enqueuer must be provided with the caracteristics of the incoming samples.
 * Will not store beyound a certain capacity.
 * 
 * The output data will be always 8192 samples, 16-bit float, 1 channel
 * 
 * NAudio provides out of the box a solution for this.
 * Perhaps that solution should be leveraged here and this abomination removed.
 * 
 */

using NAudio.Wave;
using System;
using System.Collections.Concurrent;
using System.Numerics;

namespace ScytaleC
{
    static class StreamQueueConsts
    {
        public const int MaxSampleCount = 5000; 
        public const int OutputSampleCount = 4096;
    }

    class StreamQueue: BlockingCollection<Complex[]>
    {
        private WaveFormat iwf = null;
        private int maxSampleCount = StreamQueueConsts.MaxSampleCount;
        private Complex[] output = new Complex[StreamQueueConsts.OutputSampleCount];
        private int outputPos = 0;
        private int diff = StreamQueueConsts.OutputSampleCount;

        public void SetInputDataType(WaveFormat wf)
        {
            iwf = wf;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="length"></param>
        /// <param name="counter"></param>
        /// <param name="isWave"></param>
        /// <param name="Channel">0 = mono, 1 = left, 2 = right, 3 = IQ</param>
        public void Store(byte[] buffer, int length, int counter, int Channel)
        {
            System.Threading.SpinWait.SpinUntil(() => base.Count < maxSampleCount);

            if (iwf == null)
            {
                throw new Exception("Must specify InputStreamType.");
            }

            //format incoming

            //The number of bytes for one sample including all channels.
            int truelen = 0;
            Complex[] source = null;
            /// The block align represents number of bytes used by a single sample,
            /// including data for both channels if the data is stereo. For example,
            /// the block alignment for 16-bit stereo PCM is 4 bytes (2 channels — 2 bytes per sample).
            switch (iwf.BlockAlign)
            {
                case 2:
                    //16 bit mono
                    if (iwf.Channels == 1)
                    {
                        truelen = length / 2;
                        source = new Complex[truelen];
                        for (int i = 0; i < length; i += 2)
                        {
                            double val = ((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i];
                            source[i / 2] = new Complex(val, val);
                        }
                    }
                    else
                    {
                        throw new Exception("Unsupported input wave format.");
                    }

                    break;

                case 4:
                    //16 bit stereo
                    if (iwf.Channels == 1)
                    {
                        throw new Exception("Unsupported input wave format.");
                    }
                    else
                    {
                        switch (Channel)
                        {
                            case 0:
                            case 1:
                                //16 bit left channel
                                truelen = length / 4;
                                source = new Complex[truelen];
                                for (int i = 0; i < length; i += 4)
                                {
                                    //use only left channel, first 2 samples
                                    double val = ((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i];
                                    source[i / 4] = new Complex(val, val);
                                }

                                break;

                            case 2:
                                //16 bit right channel
                                truelen = length / 4;
                                source = new Complex[truelen];
                                for (int i = 0; i < length; i += 4)
                                {
                                    //use only right channel, last 2 samples
                                    double val = ((sbyte)buffer[i + 3] << 8) + (sbyte)buffer[i + 2];
                                    source[i / 4] = new Complex(val, val);
                                }

                                break;

                            case 3:
                                //Filer.WriteFloats(50000f, 50000f);
                                //16 bit IQ data
                                truelen = length / 2;
                                source = new Complex[truelen];
                                for (int i = 0; i < length; i += 4)
                                {
                                    //Re on left channel
                                   // source[i / 2] = (double)(((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i]);

                                    //Im on right channel
                                    //source[(i / 2) + 1] = (double)(((sbyte)buffer[i + 3] << 8) + (sbyte)buffer[i + 2]);

                                    source[i / 4] = new Complex((double)(((sbyte)buffer[i + 1] << 8) + (sbyte)buffer[i]), (double)(((sbyte)buffer[i + 3] << 8) + (sbyte)buffer[i + 2]));

                                    //Debug.WriteLine("source[{0} ... {1}] = [{2} ... {3}]", i / 2, (i / 2) + 1, source[i / 2], source[(i / 2) + 1]);
                                    //Filer.WriteFloats(source[i / 2], source[(i / 2) + 1]);
                                }
                                //Filer.Flush();

                                break;
                        }
                    }

                    break;
                default:
                    throw new Exception("Unsupported input wave format.");
            }

            Store(source, 0, source.Length);
        }

        private void Store(Complex[] source, int sourcePos, int length)
        {
            if (length == diff)
            {
                Array.Copy(source, sourcePos, output, outputPos, diff);
                base.Add(output);

                output = new Complex[StreamQueueConsts.OutputSampleCount];
                sourcePos = 0;
                outputPos = 0;
                diff = StreamQueueConsts.OutputSampleCount;
            }
            else if (length > diff)
            {
                Array.Copy(source, sourcePos, output, outputPos, diff);
                base.Add(output);

                output = new Complex[StreamQueueConsts.OutputSampleCount];
                sourcePos += diff;
                outputPos = 0;
                int remaining = length - diff;
                diff = StreamQueueConsts.OutputSampleCount;

                Store(source, sourcePos, remaining);
            }
            else //source.Length < diff
            {
                Array.Copy(source, sourcePos, output, outputPos, length);

                outputPos += length;
                sourcePos = 0;
                diff = output.Length - outputPos;
            }
        }
    }

    class InputStreamType
    {
        public int SampleRate;
        public int BitDepth;
        public int Channels;
        public bool LittleEndian;

        public InputStreamType(int samplerate, int bitdepth, int channels, bool littleendian)
        {
            SampleRate = samplerate;
            BitDepth = bitdepth;
            Channels = channels;
            LittleEndian = littleendian;
        }
    }
}
