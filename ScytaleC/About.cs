﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScytaleC
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void rtbCredits_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", e.LinkText);
        }

        private void About_Load(object sender, EventArgs e)
        {
            try
            {
                rtbCredits.LoadFile(AppDomain.CurrentDomain.BaseDirectory + "about.rtf", RichTextBoxStreamType.PlainText);
            }
            catch (Exception ex)
            {
                rtbCredits.Text = ex.Message;
            }
        }
    }
}
