﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 * 
 *   Calcutt, D. M., & Tetley, L. (2004). Satellite communications: principles and applications. Oxford: Elsevier.
 *   https://www.amazon.com/Satellite-Communications-Applications-David-Calcutt/dp/034061448X
 *   
 *   Nera. (2015) Nera Inmarsat-C Service Manual. Billingstad: Nera ASA.
 *   https://www.manualslib.com/manual/1201514/Nera-Inmarsat-C.html
 * 
 * The interleave matrix (block) consists of 64 rows by 162 columns.
 * The matrix holds 10368 symbols.
 * 
 * The deinterleaver matrix contains 64 rows and only 160 columns as the UW are removed.
 * The deinterleaver matrix contains 10240 symbols.
 * 
 */

using System.Diagnostics;
using System.Threading.Tasks.Dataflow;

namespace ScytaleC.Decoder
{
    /// <summary>
    /// At the end of processing, the output will contain 10240 data symbols representing the received
    /// output of the transmitter's convolutional encoder
    /// </summary>
    class Deinterleaver
    {
        private byte[,] deinterleverMatrix;

        public async void Decode(BufferBlock<DepermutedFrameArgs> depermutedFrameBuffer, BufferBlock<DeinterleavedFrameArgs> deinterleavedFrameBuffer)
        {
            while (await depermutedFrameBuffer.OutputAvailableAsync())
            {
                DepermutedFrameArgs args = depermutedFrameBuffer.Receive();

                //create an interleaving matrix
                //we do not need space for the UW as we will be descarding them at this stage
                deinterleverMatrix = new byte[64, 160];

                //store depermutedFrame into the deinterleaver matrix leaving out the UW
                int row = -1;
                int column = 0;
                for (int i = 0; i < args.Length; i++)
                {
                    //at over 162 symbols: reset column, increment row and jump over the UW
                    if (i % 162 == 0)
                    {
                        column = 0;
                        row++;
                        i += 2;
                    }

                    deinterleverMatrix[row, column] = args.DepermutedFrame[i];
                    column++;
                }

                //create the destination
                byte[] destination = new byte[DataConsts.DeinterleavedFrameLength];

                //read the matrix into the destination, transposed
                int pos = 0;
                row = 0;
                column = 0;
                for (; row < 64;)
                {
                    destination[pos] = deinterleverMatrix[row, column];

                    row++;
                    if (row % 64 == 0)
                    {
                        row = 0;
                        column++;

                        if (column == 160)
                        {
                            break;
                        }
                    }

                    pos++;
                }

                DeinterleavedFrameArgs dfa = new DeinterleavedFrameArgs
                {
                    Length = DataConsts.DeinterleavedFrameLength,
                    DeinterleavedFrame = destination,
                    IsHardDecision = args.IsHardDecision
                };
                deinterleavedFrameBuffer.Post(dfa);
            }
        }
    }
}
