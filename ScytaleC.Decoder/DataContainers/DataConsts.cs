﻿/*
 * microp11 2018
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Constants used throughout the application.
 * 
 */

namespace ScytaleC.Decoder
{
    public class DataConsts
    {
        public static readonly int MaxTolerance = 30;
        public static readonly int UWFinderFrameLength = 10368;
        public static readonly int DepermutedFrameLength = 10368;
        public static readonly int DeinterleavedFrameLength = 10240;
        public static readonly int ViterbiDecodedFrameLength = 640;
        public static readonly int DescrambledFrameLength = 640;

        public static readonly double InmarsatCCenterFrequency = 2000.0;

        public static readonly int LoFrequency = 900;
        public static readonly int HiFrequency = 2500;

        public static readonly int SamplesPerRead = 8192;
        public static readonly int AudioSampleRate = 48000;
        public static readonly int AudioBitsPerSample = 16;
    }
}