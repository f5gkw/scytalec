﻿/*
 * microp11 2019
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ScytaleC
{
    public static class CoordinatesList
    {
        private static readonly string _pattern = "(\\d{1,3})(-| |.)(\\d{1,3})(((.|,)(\\d{1,3})( *))*)(N|S)( *)(-*)( *)(\\d{1,3})(-| |.)(\\d{1,3})(((.|,)(\\d{1,3})( *))*)(E|W)";
        private static readonly MatchEvaluator _evaluator = new MatchEvaluator(Evaluator);

        public static List<Coordinates> List { get; set; } = new List<Coordinates>();

        public static bool TryParseCoordinates(string input)
        {
            List.Clear();
            try
            {
                Regex.Replace(input, _pattern, _evaluator);
            }
            catch {}
            return List.Count > 0;
        }

        private static string Evaluator(Match m)
        {
            try
            {
                Coordinates coordinates = new Coordinates();

                //latitude
                int.TryParse(m.Groups[1].Value, out int degrees);
                coordinates.LatDegrees = degrees;

                int.TryParse(m.Groups[3].Value, out int minutes);
                coordinates.LatMinutes = minutes;

                int.TryParse(m.Groups[7].Value, out int seconds);
                coordinates.LatSeconds = seconds;

                coordinates.LatHemisphere = m.Groups[9].Value.ToUpper() == "N" ? Coordinates.NORTHERN : Coordinates.SOUTHERN;

                //longitude
                int.TryParse(m.Groups[13].Value, out degrees);
                coordinates.LngDegrees = degrees;

                int.TryParse(m.Groups[15].Value, out minutes);
                coordinates.LngMinutes = minutes;

                int.TryParse(m.Groups[19].Value, out seconds);
                coordinates.LngSeconds = seconds;

                coordinates.LngHemisphere = m.Groups[21].Value.ToUpper() == "E" ? Coordinates.EASTERN : Coordinates.WESTERN;

                coordinates.Value = m.Value;
                coordinates.ConvertDegreeAngleToDouble();

                List.Add(coordinates);

                return m.Value;
            }
            catch
            {
                return "";
            }
        }
    }

    public class Coordinates
    {
        public static int NORTHERN = 0;
        public static int SOUTHERN = 1;
        public static int EASTERN = 2;
        public static int WESTERN = 3;
        public string Value { get; set; }

        public int LatDegrees { get; set; }
        public int LatMinutes { get; set; }
        public int LatSeconds { get; set; }
        public int LatHemisphere { get; set; }

        public int LngDegrees { get; set; }
        public int LngMinutes { get; set; }
        public int LngSeconds { get; set; }
        public int LngHemisphere { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }

        public void ConvertDegreeAngleToDouble()
        {
            Lat = ConvertDegreeAngleToDouble(LatDegrees, LatMinutes, LatSeconds);
            Lat = LatHemisphere == NORTHERN ? Lat : -Lat;

            Lng = ConvertDegreeAngleToDouble(LngDegrees, LngMinutes, LngSeconds);
            Lng = LngHemisphere == EASTERN ? Lng : -Lng;

        }
        private double ConvertDegreeAngleToDouble(double degrees, double minutes, double seconds)
        {
            var multiplier = degrees < 0 ? -1 : 1;
            var _deg = Math.Abs(degrees);
            var result = _deg + (minutes / 60) + (seconds / 3600);
            return result * multiplier;
        }
    }
}
