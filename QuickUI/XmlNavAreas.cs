﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *   Bibliography:
 *   
 *   https://www.navcen.uscg.gov/pdf/gmdss/Safety_NET_Manual.pdf
 *   
 *   The colors can be any combination of the c# colors found in this table:
 *   http://www.flounder.com/csharp_color_table.htm
 *
 *   The Nav Areas are numbered from 1 to 21. The Nav Areas can be found at:
 *   https://www.iho.int/mtg_docs/com_wg/CPRNW/WWNWS4/WWNWS4.htm
 *
 *   The coordinates must describe a clockwise polygonal area.
 * 
 *    
 */


using ScytaleC.PacketDecoders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace ScytaleC.QuickUI
{
    public class XmlNavAreas
    {
        private string fullPathToNavAreasXml;
        private List<XmlNavArea> navAreaList;

        public XmlNavAreas(string location)
        {
            fullPathToNavAreasXml = string.Format(@"{0}\NavAreas.xml", location);
            navAreaList = new List<XmlNavArea>();
        }

        public void LoadNavAreasFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<XmlNavArea>));

            try
            {
                using (var reader = new StreamReader(fullPathToNavAreasXml))
                {
                    navAreaList = (List<XmlNavArea>)serializer.Deserialize(reader);
                }
            }
            catch (FileNotFoundException)
            {
                CreateXmlFile();
            }

            foreach (XmlNavArea area in navAreaList)
            {
                try
                {
                    area.BkgColor = Color.FromName(area.BackgroundColor);
                    area.PrmColor = Color.FromName(area.PerimeterColor);
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);

                    //set a default color
                    area.BkgColor = Color.LightBlue;
                    area.PrmColor = Color.DarkBlue;
                }
            }
        }

        public XmlNavArea ReturnXmlNavArea(int areaId)
        {
            return navAreaList.FirstOrDefault(x => x.Id == areaId);
        }

        private void CreateXmlFile()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<NavArea>));

            using (var writer = new StreamWriter(fullPathToNavAreasXml))
            {
                serializer.Serialize(writer, navAreaList);
            }
        }
    }
}
