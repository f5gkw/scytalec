﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScytaleC.QuickUI
{
    public partial class Disclaimer : Form
    {
        public bool DontShowAgainChecked { get; private set; }
        public bool BtnNoPressed { get; private set; }

        public Disclaimer()
        {
            InitializeComponent();
            DontShowAgainChecked = false;
            BtnNoPressed = false;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            DontShowAgainChecked = cbDoNotShowAgain.Checked;
            Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            BtnNoPressed = true;
            Close();
        }
    }
}
